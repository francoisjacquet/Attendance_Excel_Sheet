��          T      �       �      �   3   �   *     )   /  0   Y     �  �  �     �  ?   �  =   �  9   ,  .   f  "   �                                        Attendance Excel Sheet Create Attendance Sheet for Selected Course Periods Error: AttendanceSheet.xls file not found. Error: Cannot save temporary Excel sheet. PHP zip extension is required. Please enable it. Print Attendance Sheets Project-Id-Version: Attendance Excel Sheet Module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 16:47+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Feuille de présence Excel Créer la feuille de présence pour les classes sélectionnées Erreur: le fichier AttendanceSheet.xls n'a pas été trouvé. Erreur: impossible de sauver la feuille Excel temporaire. Extension PHP zip requise. Merci de l'activer. Imprimer les feuilles de présence 